#! /usr/bin/env false

use v6.d;

use Config;
use IO::Glob;
use IO::Path::XDG;
use IRC::Client;
use IRC::Client::Plugin::DiceRolls;
use IRC::Client::Plugin::Memos;
use IRC::Client::Plugin::NickServ;
use IRC::Client::Plugin::Reminders;
use IRC::TextColor;
use Log;
use Log::Level;

unit module Local::Musashi;

#| Run the musashi IRC bot.
unit sub MAIN (
) is export {
	# Set up logger
	if (%*ENV<RAKU_LOG_CLASS>:exists) {
		$Log::instance = (require ::(%*ENV<RAKU_LOG_CLASS>)).new;
		$Log::instance.add-output($*OUT, %*ENV<RAKU_LOG_LEVEL> // Log::Level::Info);
	}

	# Create $bot early to use it in traps
	my IRC::Client $bot;

	# Play nice with Kubernetes
	$*ERR.out-buffer = False;
	$*OUT.out-buffer = False;

	# Trap signals
	signal(SIGTERM).tap({ .quit with $bot });

	# Load config
	my $config = Config.new({
		debug => False,
		database => {
			driver => 'postgresql',
			host => Str,
			database => 'musashi',
			user => ~$*USER,
			password => Str,
		},
		irc => {
			nickname => 'musashi',
			username => 'musashi',
			realname => 'musashi',

			host => Str,
			port => 6697,
			ssl => True,

			channels => [ ],

			command-prefix => '.',

			plugins => {
				dicerolls => {
					command-word => 'roll',
				},
				nickserv => {
					account => Str,
					password => Str,
				},
				reminders => {
					interval => Int,
				},
			},
		},
	}, :name<musashi>);

	if (!$config.get('irc.opers')) {
		.warning('No opers defined in irc.opers') with $Log::instance;
	}

	# Set up database connection
	my $*DB;

	my $db-driver = $config.get('database.driver', '').fc;

	given ($db-driver) {
		when 'postgresql' {
			try require DB::Pg;

			if (DB::Pg ~~ Failure) {
				.emergency('Failed to load DB::Pg') with $Log::instance;
				return;
			}

			my $conninfo = %(
				host => $config.get('database.host', 'localhost'),
				dbname => $config.get('database.database', 'musashi'),
				user => $config.get('database.user', ~$*USER),
				password => $config.get('database.password'),
				port => $config.get('database.port', 5432),
			).pairs.grep(*.value).map({ "{$_.key}={$_.value}" }).join(' ');

			.info("Using Postgres database ($conninfo)") with $Log::instance;

			$*DB = DB::Pg.new(:$conninfo);
		}
		default {
			.warning("Invalid database driver '$db-driver'") with $Log::instance;
		}
	}

	# Prepare plugins
	my @plugins = (
		# Ignore certain users.
		class {
			also is IRC::Client::Plugin;

			method bulli (
				Str() $usermask,
			) {
				for $config.get('irc.ignore', []).List -> $bully {
					next unless $usermask ~~ glob($bully);

					.notice("$bully is ignored") with $Log::instance;

					return Nil;
				}

				$.NEXT;
			}

			multi method irc-addressed ($event)       { self.bulli($event.usermask) }
			multi method irc-to-me ($event)           { self.bulli($event.usermask) }
			multi method irc-mentioned ($event)       { self.bulli($event.usermask) }
			multi method irc-privmsg-channel ($event) { self.bulli($event.usermask) }
			multi method irc-privmsg-me ($event)      { self.bulli($event.usermask) }
			multi method irc-privmsg ($event)         { self.bulli($event.usermask) }
			multi method irc-all ($event)             { self.bulli($event.usermask) }
			multi method irc-notice-channel ($event)  { self.bulli($event.usermask) }
			multi method irc-notice-me ($event)       { self.bulli($event.usermask) }
			multi method irc-notice ($event)          { self.bulli($event.usermask) }
		},

		IRC::Client::Plugin::NickServ.new(config => $config),
		IRC::Client::Plugin::DiceRolls.new(config => $config),
		IRC::Client::Plugin::Reminders.new(config => $config),
		IRC::Client::Plugin::Memos.new(config => $config),

		# Musashi-specific behaviour
		class {
			multi method irc-privmsg-channel($e where /^hi$/)
			{
				"Hi {$e.nick}!"
			}

			multi method irc-privmsg-channel($ where /^o\/$/)
			{
				"\\o"
			}

			multi method irc-privmsg-channel($e where /^[good]?morn[ing]?$/)
			{
				"And a good morning to you too, {$e.nick}"
			}

			#| Rewrite Twitter links
			multi method irc-privmsg-channel($e where *.text.contains('twitter.com'))
			{
				my $alternative = <
					nitter.42l.fr
					nitter.eu
					nitter.fdn.fr
					nitter.namazso.eu
					nitter.nixnet.services
					nitter.pussthecat.org
					nitter.tedomum.net
					nitter.unixfox.eu
					prvy.top
				>.pick();

				for $e.text.words.grep(*.contains('twitter.com')) -> $link {
					.debug("Rewriting $link to $alternative") with $Log::instance;

					$e.irc.send(
						text => '> ' ~ $link.subst('twitter.com', $alternative),
						where => $e.channel,
					);
				}

				Nil;
			}

			#| Allow reloading configuration while the bot is
			#| running.
			multi method irc-to-me (
				$event where 'reload',
			) {
				if (!self!is-oper($event.usermask)) {
					return "I'm sorry, I can't let you do that";
				}

				.info('Reloading configuration') with $Log::instance;
				.error('Not Properly Implemented!') with $Log::instance;

				for @plugins -> $plugin {
					next unless $plugin.^can('reload-config');

					.info("Reloading plugin $plugin") with $Log::instance;
					$plugin.reload-config($config);
				}

				'Reloaded configuration!';
			}

			#| Check if a usermask has oper privileges for the bot.
			method !is-oper (
				Str() $usermask,
				--> Bool:D
			) {
				for $config.get('irc.opers', []).List -> $oper {
					return True if $usermask ~~ glob($oper);
				}

				False;
			}
		},
	);

	# Set up the bot
	$bot .= new(
		:nick($config.get("irc.nickname", "musashi"))
		:username($config.get("irc.username", "musashi"))
		:realuser($config.get("irc.realname", "Yet another tachikoma AI"))
		:host($config.get("irc.host", "irc.darenet.org"))
		:port($config.get("irc.port", 6667))
		:ssl($config.get("irc.ssl", False))
		:channels($config.get("irc.channels", "#scriptkitties"))
		:debug($config.get("debug", True))
		:!autoprefix
		:@plugins
	);

	# Start
	$bot.run;
}

=begin pod

=NAME    Local::Musashi
=AUTHOR  Patrick Spek <p.spek@tyil.work>
=VERSION 0.0.1

=head1 Synopsis

=head1 Description

=head1 Examples

=head1 See also

=end pod

# vim: ft=perl6 noet
